# Sally

一个轻量级的工作流引擎，专注于工作流的使用而非配置。

## 📖 背景

国内项目普遍会遇到"工作流"业务，比如请假、项目审核等。  
但是这种业务本质上分为两个部分：配置工作流和使用工作流。

配置工作流这部分，只是提到这个词，我们脑中立刻会出现节点、连线这样的场景，这部分业务对于前端和后端来讲都是比较复杂的。
当前市面上主流的workflow框架，都包含了配置工作流功能，功能一个比一个强劲，但是也一个比一个复杂。  

研究了多个go语言编写的workflow框架后，我发现了以下问题：  

1. 对于只想"使用工作流"的场景，如果引入了开源workflow，需要耗费大量精力在理解"配置工作流"这部分逻辑。
2. 开源workflow框架，把大量业务沉到了数据库层，所以对存储设备有具体的要求。这并不符合"面向对象"。
3. 在自研产品中使用开源workflow，等于把一个关键的业务绑定在了开源项目作者的身上，极大的限制了产品经理的发挥。

## 🔍 概念介绍

| 概念 | 说明 |
|------|------|
| nodeTemplate | 节点模板，节点就是工作流中的一个"步骤" |
| workflowTemplate | 工作流模板，工作流就是一组节点的集合，每个节点之间通过连线连接起来 |
| 模板 | 用户配置好的工作流模板，可以复用。启动一个工作流，就是从模板中启动一个实例，这个概念类似于docker中的image和container |
| node | 节点，启动工作流后，生成具体的节点 |
| workflow | 启动的工作流，一连串node组成的实例 |
| reviewer | 审核人，和nodeTemplate关联 |
| reviewerTask | 审核人任务，当流转到对应的node时，创建的审批任务 |

## 🚀 快速开始

### 初始化项目

```go
db, err := connect(Config{
    Host:     "your-host",
    Port:     "3306",
    User:     "root",
    Password: "your-password",
    Schema:   "sally",
})

clerkService, err := Init(db, &hook{
    GetReviewerIDs: func(ctx context.Context, reviewerNos []string, formsContent string, params aggregate.BusinessParams) (userIDs []string, err error) {
        return []string{"Nash", "admin"}, nil
    },
    HandlePassedWorkflow: func(ctx context.Context, businessID, formContent string, params aggregate.BusinessParams, businessCode string, tasks []aggregate.NodeReviewTask) (err error) {
        return nil
    },
    HandleRejectedWorkflow: func(ctx context.Context, businessID, formContent string, params aggregate.BusinessParams, businessCode string) (err error) {
        return nil
    },
    NotifyNextReviewers: func(ctx context.Context, businessID, formContent string, params aggregate.BusinessParams, businessCode string, nextNodeReviewerIDs []string) (err error) {
        return nil
    },
})
```

### 工作流操作

#### 启动工作流

```go
startWorkFlowRequest := aggregate.StartWorkFlowRequest{
    BusinessCode:       "agenda",
    BusinessID:         "agenda-001",
    FormContent:        `{"name":"meng"}`,
    SponsorID:          "admin",
    WorkflowTemplateID: "MonthlyReport",
    BusinessParams: map[string]any{
        "dameng": "eric",
    },
}
err = clerkService.StartWorkflow(context.Background(), startWorkFlowRequest)
```

#### 重启工作流

```go
restartWorkFlowRequest := aggregate.RestartWorkFlowRequest{
    BusinessCode: "agenda",
    BusinessID:   "agenda-001",
    FormContent:  `{"name":"fu"}`,
    SponsorID:    "admin",
    WorkflowID:   workflowID,
}
err := clerkService.RestartWorkflow(context.Background(), restartWorkFlowRequest)
```

#### 审批

```go
reviewRequest := aggregate.ReviewRequest{
    Comment:            "pass",
    NodeReviewerTaskID: "0192f5ca-0ad9-7ad9-b7d6-5e7f3a0eab53",
    Status:             aggregate.ReviewPassed,
}
err = clerkService.Review(context.Background(), reviewRequest)
```

### 查询功能

#### 查询工作流列表

```go
request := aggregate.GetWorkflowsRequest{
    SponsorID:    "admin",
    BusinessCode: "agenda",
    BusinessID:   "agenda-001",
    PageCond: aggregate.PageCond{
        PageNo:   1,
        PageSize: 10,
    },
    CommonSearchCond: aggregate.CommonSearchCond{
        Search: "meng",
    },
}
result, total, err := clerkService.GetWorkflows(context.Background(), request)
```

#### 查询单个工作流

```go
result, err := clerkService.GetWorkflow(context.Background(), workflowID)
```

#### 查询审批任务

```go
request := aggregate.GetNodeReviewTasksRequest{
    ReviewerID:   "admin",
    BusinessCode: "agenda",
    BusinessID:   "agenda-001",
    PageCond: aggregate.PageCond{
        PageNo:   1,
        PageSize: 10,
    },
    CommonSearchCond: aggregate.CommonSearchCond{
        Search: "meng",
    },
}
result, total, err := clerkService.GetNodeReviewTasks(context.Background(), request)
```

### 其他操作

#### 更新工作流内容

```go
err := clerkService.UpdateWorkflowContent(context.Background(), workflowID, `{"name":"fu"}`, aggregate.BusinessParams{
    "xiaomeng": "eric",
})
```

#### 删除工作流

```go
err := clerkService.RemoveWorkflow(context.Background(), workflowID)
```