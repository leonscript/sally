package sally

import (
	"context"
	"errors"
	"fmt"
	"os"
	"slices"
	"testing"

	"gitee.com/leonscript/sally/domain/aggregate"
	"gitee.com/leonscript/sally/domain/entity"
	"gitee.com/leonscript/sally/infrastructure/persistent"
	"github.com/smartystreets/goconvey/convey"
)

func TestMain(m *testing.M) {
	// 在所有测试开始之前可以添加一些初始化代码
	fmt.Println("TestMain is running")
	code := m.Run()
	// 在所有测试结束后执行清理操作
	CleanTestData()
	// 返回测试结果
	os.Exit(code)
}

func TestClerk_StartWorkflow(t *testing.T) {
	convey.Convey("test init", t, func() {
		clerkService, err := getClerkService()
		convey.So(err, convey.ShouldBeNil)

		convey.Convey("test start workflow", func() {
			startWorkFlowRequest := aggregate.StartWorkFlowRequest{
				BusinessCode:       "agenda",
				BusinessID:         "agenda-001",
				FormContent:        `{"name":"meng"}`,
				SponsorID:          "admin",
				WorkflowTemplateID: "MonthlyReport",
				BusinessParams: map[string]any{
					"dameng": "eric",
				},
			}
			err = clerkService.StartWorkflow(context.Background(), startWorkFlowRequest)
			convey.So(err, convey.ShouldBeNil)
		})
	})
}

func TestClerk_StartWorkflowWithInvalidTemplate(t *testing.T) {
	convey.Convey("test init", t, func() {
		clerkService, err := getClerkService()
		convey.So(err, convey.ShouldBeNil)

		convey.Convey("test start workflow with invalid template", func() {
			startWorkFlowRequest := aggregate.StartWorkFlowRequest{
				BusinessCode:       "agenda",
				BusinessID:         "agenda-001",
				FormContent:        `{"name":"meng"}`,
				SponsorID:          "admin",
				WorkflowTemplateID: "InvalidTemplate",
				BusinessParams: map[string]any{
					"dameng": "eric",
				},
			}
			err = clerkService.StartWorkflow(context.Background(), startWorkFlowRequest)
			convey.So(err, convey.ShouldNotBeNil)
		})
	})
}

func TestClerk_ReviewReject(t *testing.T) {
	clerkService, err := getClerkService()

	var task aggregate.NodeReviewTask
	convey.Convey("test query tasks", t, func() {
		request := aggregate.GetNodeReviewTasksRequest{
			ReviewerID:   "Nash",
			BusinessCode: "agenda",
			BusinessID:   "agenda-001",
			PageCond: aggregate.PageCond{
				PageNo:   1,
				PageSize: 10,
			},
			CommonSearchCond: aggregate.CommonSearchCond{
				Search: "meng",
			},
		}
		tasks, total, err := clerkService.GetNodeReviewTasks(context.Background(), request)
		convey.So(err, convey.ShouldBeNil)
		convey.So(total, convey.ShouldNotEqual, 0)
		convey.So(len(tasks), convey.ShouldEqual, total)
		i := slices.IndexFunc(tasks, func(taskPO aggregate.NodeReviewTask) bool {
			return taskPO.Status == entity.TaskUnderReviewStatus
		})
		task = tasks[i]
	})

	// 第一次审批驳回
	convey.Convey("test first review reject", t, func() {
		reviewRequest := aggregate.ReviewRequest{
			Comment:            "reject",
			NodeReviewerTaskID: task.ID,
			Status:             aggregate.ReviewRejected,
		}
		err = clerkService.Review(context.Background(), reviewRequest)
		convey.So(err, convey.ShouldBeNil)
	})

	// 使用相同的任务ID进行重复审批
	convey.Convey("test duplicate review should fail", t, func() {
		reviewRequest := aggregate.ReviewRequest{
			Comment:            "reject again",
			NodeReviewerTaskID: task.ID,
			Status:             aggregate.ReviewRejected,
		}
		err = clerkService.Review(context.Background(), reviewRequest)
		convey.So(err, convey.ShouldNotBeNil)
	})
}

func TestClerk_RestartWorkflow(t *testing.T) {
	clerkService, _ := getClerkService()
	var workflowID string
	// 查询工作流
	convey.Convey("test get workflows", t, func() {
		request := aggregate.GetWorkflowsRequest{
			SponsorID:    "admin",
			BusinessCode: "agenda",
			BusinessID:   "agenda-001",
			PageCond: aggregate.PageCond{
				PageNo:   1,
				PageSize: 10,
			},
			CommonSearchCond: aggregate.CommonSearchCond{
				Search: "meng",
			},
		}
		result, total, err := clerkService.GetWorkflows(context.Background(), request)
		convey.So(err, convey.ShouldBeNil)
		convey.So(total, convey.ShouldNotEqual, 0)
		convey.So(len(result), convey.ShouldEqual, total)
		workflowID = result[0].ID
	})

	// 重新启动工作流
	convey.Convey("test get workflows", t, func() {
		restartWorkFlowRequest := aggregate.RestartWorkFlowRequest{
			BusinessCode: "agenda",
			BusinessID:   "agenda-001",
			FormContent:  `{"name":"fu"}`,
			SponsorID:    "admin",
			WorkflowID:   workflowID,
		}
		err := clerkService.RestartWorkflow(context.Background(), restartWorkFlowRequest)
		convey.So(err, convey.ShouldBeNil)
	})

	// 查询工作流
	convey.Convey("test get workflows", t, func() {
		request := aggregate.GetWorkflowsRequest{
			SponsorID:    "admin",
			BusinessCode: "agenda",
			BusinessID:   "agenda-001",
			PageCond: aggregate.PageCond{
				PageNo:   1,
				PageSize: 10,
			},
			CommonSearchCond: aggregate.CommonSearchCond{
				Search: "fu",
			},
		}
		result, total, err := clerkService.GetWorkflows(context.Background(), request)
		convey.So(err, convey.ShouldBeNil)
		convey.So(total, convey.ShouldNotEqual, 0)
		convey.So(len(result), convey.ShouldEqual, total)
		workflowID = result[0].ID
	})

	// 更新工作流
	convey.Convey("test update workflow", t, func() {
		err := clerkService.UpdateWorkflowContent(context.Background(), workflowID, `{"name":"fu"}`, aggregate.BusinessParams{
			"xiaomeng": "eric",
		})
		convey.So(err, convey.ShouldBeNil)
	})
}

func TestClerk_ReviewPass(t *testing.T) {
	clerkService, err := getClerkService()
	var task aggregate.NodeReviewTask
	convey.Convey("test query tasks", t, func() {
		request := aggregate.GetNodeReviewTasksRequest{
			ReviewerID:   "Nash",
			BusinessCode: "agenda",
			BusinessID:   "agenda-001",
			PageCond: aggregate.PageCond{
				PageNo:   1,
				PageSize: 10,
			},
			CommonSearchCond: aggregate.CommonSearchCond{
				Search: "fu",
			},
		}
		tasks, total, err := clerkService.GetNodeReviewTasks(context.Background(), request)
		convey.So(err, convey.ShouldBeNil)
		convey.So(total, convey.ShouldNotEqual, 0)
		convey.So(len(tasks), convey.ShouldEqual, total)
		i := slices.IndexFunc(tasks, func(taskPO aggregate.NodeReviewTask) bool {
			return taskPO.Status == entity.TaskUnderReviewStatus
		})
		task = tasks[i]
	})

	convey.Convey("test review pass", t, func() {
		reviewRequest := aggregate.ReviewRequest{
			Comment:            "pass",
			NodeReviewerTaskID: task.ID,
			Status:             aggregate.ReviewPassed,
		}
		err = clerkService.Review(context.Background(), reviewRequest)
		convey.So(err, convey.ShouldBeNil)
	})
}

func TestClerk_GetWorkflow(t *testing.T) {
	clerkService, _ := getClerkService()
	// 查询工作流列表获取workflowID
	var workflowID string
	convey.Convey("test get workflows", t, func() {
		request := aggregate.GetWorkflowsRequest{
			SponsorID:    "admin",
			BusinessCode: "agenda",
			BusinessID:   "agenda-001",
			PageCond: aggregate.PageCond{
				PageNo:   1,
				PageSize: 10,
			},
			CommonSearchCond: aggregate.CommonSearchCond{
				Search: "fu",
			},
		}
		result, total, err := clerkService.GetWorkflows(context.Background(), request)
		convey.So(err, convey.ShouldBeNil)
		convey.So(total, convey.ShouldNotEqual, 0)
		convey.So(len(result), convey.ShouldEqual, total)
		workflowID = result[0].ID
	})

	// 测试正常获取工作流
	convey.Convey("test get workflow success", t, func() {
		result, err := clerkService.GetWorkflow(context.Background(), workflowID)
		convey.So(err, convey.ShouldBeNil)
		convey.So(result.ID, convey.ShouldEqual, workflowID)
		convey.So(result.FormContent, convey.ShouldNotBeEmpty)
		convey.So(result.CurrentNodeID, convey.ShouldNotBeEmpty)
		convey.So(result.Status, convey.ShouldNotEqual, 0)
		convey.So(result.BusinessParams, convey.ShouldNotBeNil)
	})
}

func TestClerk_RemoveWorkflow(t *testing.T) {
	clerkService, _ := getClerkService()
	// 查询工作流
	var workflowID string
	convey.Convey("test get workflows", t, func() {
		request := aggregate.GetWorkflowsRequest{
			SponsorID:    "admin",
			BusinessCode: "agenda",
			BusinessID:   "agenda-001",
			PageCond: aggregate.PageCond{
				PageNo:   1,
				PageSize: 10,
			},
			CommonSearchCond: aggregate.CommonSearchCond{
				Search: "fu",
			},
		}
		result, total, err := clerkService.GetWorkflows(context.Background(), request)
		convey.So(err, convey.ShouldBeNil)
		convey.So(total, convey.ShouldNotEqual, 0)
		convey.So(len(result), convey.ShouldEqual, total)
		workflowID = result[0].ID
	})

	// 移除工作流
	convey.Convey("test remove workflow", t, func() {
		err := clerkService.RemoveWorkflow(context.Background(), workflowID)
		convey.So(err, convey.ShouldBeNil)
	})

	// 查询工作流
	convey.Convey("test get workflows", t, func() {
		request := aggregate.GetWorkflowsRequest{
			SponsorID:    "admin",
			BusinessCode: "agenda",
			BusinessID:   "agenda-001",
			PageCond: aggregate.PageCond{
				PageNo:   1,
				PageSize: 10,
			},
			CommonSearchCond: aggregate.CommonSearchCond{
				Search: "fu",
			},
		}
		result, total, err := clerkService.GetWorkflows(context.Background(), request)
		convey.So(err, convey.ShouldBeNil)
		convey.So(total, convey.ShouldEqual, 0)
		convey.So(len(result), convey.ShouldEqual, total)
	})
}

func CleanTestData() {
	db, _ := connect(Config{
		Host:     "192.168.110.242",
		Port:     "3306",
		User:     "root",
		Password: "Ul6WI12AuZomj76Kvl700-",
		Schema:   "sally",
	})
	db.Where("id IS NOT NULL").Delete(&persistent.Workflow{})
	db.Where("id IS NOT NULL").Delete(&persistent.Node{})
	db.Where("id IS NOT NULL").Delete(&persistent.NodeReviewerTask{})
}

func getClerkService() (result aggregate.ClerkService, err error) {
	db, err := connect(Config{
		Host:     "192.168.110.242",
		Port:     "3306",
		User:     "root",
		Password: "Ul6WI12AuZomj76Kvl700-",
		Schema:   "sally",
	})

	clerkService, err := Init(db, &hook{})
	return clerkService, err
}

type hook struct {
}

// HandleCrearedReviewTasks implements aggregate.Hook.
func (h *hook) HandleCrearedReviewTasks(ctx context.Context, businessID string, formContent string, businessCode string, nextNodeID string, params aggregate.BusinessParams, nextNodeReviewTasks []aggregate.NodeReviewTask) (err error) {
	return err
}

// HandlePassedNode implements aggregate.Hook.
func (h *hook) HandlePassedNode(ctx context.Context, workflowID string, nodeID string, reviewTaskID string, reviewerID string) (err error) {
	return nil
}

// HandleRejectedNode implements aggregate.Hook.
func (h *hook) HandleRejectedNode(ctx context.Context, workflowID string, nodeID string, reviewTaskID string, reviewerID string) (err error) {
	return nil
}

func (h hook) HandleRejectedWorkflow(ctx context.Context, businessID, formContent string, params aggregate.BusinessParams, businessCode string) (err error) {
	return nil
}

func (h hook) GetReviewerIDs(ctx context.Context, reviewerNos []string, formsContent string, params aggregate.BusinessParams) (userIDs []string, err error) {
	return []string{"Nash", "Amare"}, nil
}

func (h hook) HandlePassedWorkflow(ctx context.Context, businessID, formContent string, params aggregate.BusinessParams, businessCode string, tasks []aggregate.NodeReviewTask) (err error) {
	return nil
}

type errorHook struct {
}

func (h errorHook) HandleRejectedWorkflow(ctx context.Context, businessID, formContent string, params aggregate.BusinessParams, businessCode string) (err error) {
	return errors.New("mock error")
}

func (h errorHook) GetReviewerIDs(ctx context.Context, reviewerNos []string, formsContent string, params aggregate.BusinessParams) (userIDs []string, err error) {
	return nil, errors.New("mock error")
}

func (h errorHook) HandlePassedWorkflow(ctx context.Context, businessID, formContent string, params aggregate.BusinessParams, businessCode string, tasks []aggregate.NodeReviewTask) (err error) {
	return errors.New("mock error")
}

// HandleCrearedReviewTasks implements aggregate.Hook.
func (h errorHook) HandleCrearedReviewTasks(ctx context.Context, businessID string, formContent string, businessCode string, nextNodeID string, params aggregate.BusinessParams, nextNodeReviewTasks []aggregate.NodeReviewTask) (err error) {
	return errors.New("mock error")
}

// HandlePassedNode implements aggregate.Hook.
func (h errorHook) HandlePassedNode(ctx context.Context, workflowID string, nodeID string, reviewTaskID string, reviewerID string) (err error) {
	return errors.New("mock error")
}

// HandleRejectedNode implements aggregate.Hook.
func (h errorHook) HandleRejectedNode(ctx context.Context, workflowID string, nodeID string, reviewTaskID string, reviewerID string) (err error) {
	return errors.New("mock error")
}

func TestClerk_StartWorkflowWithHookError(t *testing.T) {
	db, err := connect(Config{
		Host:     "192.168.110.242",
		Port:     "3306",
		User:     "root",
		Password: "Ul6WI12AuZomj76Kvl700-",
		Schema:   "sally",
	})

	clerkService, err := Init(db, &errorHook{})

	convey.Convey("test start workflow with hook error", t, func() {
		startWorkFlowRequest := aggregate.StartWorkFlowRequest{
			BusinessCode:       "agenda",
			BusinessID:         "agenda-001",
			FormContent:        `{"name":"meng"}`,
			SponsorID:          "admin",
			WorkflowTemplateID: "MonthlyReport",
			BusinessParams: map[string]any{
				"dameng": "eric",
			},
		}
		err = clerkService.StartWorkflow(context.Background(), startWorkFlowRequest)
		convey.So(err, convey.ShouldNotBeNil)
		convey.So(err.Error(), convey.ShouldEqual, "mock error")
	})
}

func TestClerk_AutoReview(t *testing.T) {
	clerkService, err := getClerkService()

	// 测试场景1：下一级审批人包含当前审批人
	convey.Convey("test auto review when next reviewer includes current reviewer", t, func() {
		// 启动工作流
		startWorkFlowRequest := aggregate.StartWorkFlowRequest{
			BusinessCode:       "agenda",
			BusinessID:         "agenda-002",
			FormContent:        `{"name":"auto"}`,
			SponsorID:          "admin",
			WorkflowTemplateID: "MonthlyReport",
			BusinessParams: map[string]any{
				"test": "auto",
			},
		}
		err = clerkService.StartWorkflow(context.Background(), startWorkFlowRequest)
		convey.So(err, convey.ShouldBeNil)

		// 查询任务
		request := aggregate.GetNodeReviewTasksRequest{
			ReviewerID:   "Nash",
			BusinessCode: "agenda",
			BusinessID:   "agenda-002",
			PageCond: aggregate.PageCond{
				PageNo:   1,
				PageSize: 10,
			},
			CommonSearchCond: aggregate.CommonSearchCond{
				Search: "auto",
			},
		}
		tasks, total, err := clerkService.GetNodeReviewTasks(context.Background(), request)
		convey.So(err, convey.ShouldBeNil)
		convey.So(total, convey.ShouldNotEqual, 0)
		convey.So(len(tasks), convey.ShouldEqual, total)

		// 找到待审批的任务
		i := slices.IndexFunc(tasks, func(taskPO aggregate.NodeReviewTask) bool {
			return taskPO.Status == entity.TaskUnderReviewStatus
		})
		task := tasks[i]

		// 审批通过第一个任务
		reviewRequest := aggregate.ReviewRequest{
			Comment:            "pass",
			NodeReviewerTaskID: task.ID,
			Status:             aggregate.ReviewPassed,
		}
		err = clerkService.Review(context.Background(), reviewRequest)
		convey.So(err, convey.ShouldBeNil)

		// 查询下一个节点的任务，应该已经被自动审批通过
		tasks, total, err = clerkService.GetNodeReviewTasks(context.Background(), request)
		convey.So(err, convey.ShouldBeNil)
		convey.So(total, convey.ShouldNotEqual, 0)
		for _, task := range tasks {
			if task.ReviewerID == "admin" {
				convey.So(task.Status, convey.ShouldEqual, entity.TaskPassedStatus)
			}
		}
	})

	// 测试场景2：下一级审批人不包含当前审批人
	convey.Convey("test auto review when next reviewer does not include current reviewer", t, func() {
		// 创建一个新的TestHook结构体实现Hook接口
		testHook := &TestHook{}
		// 使用新的TestHook初始化服务
		db, err := connect(Config{
			Host:     "192.168.110.242",
			Port:     "3306",
			User:     "root",
			Password: "Ul6WI12AuZomj76Kvl700-",
			Schema:   "sally",
		})
		convey.So(err, convey.ShouldBeNil)

		clerkService, err = Init(db, testHook)
		convey.So(err, convey.ShouldBeNil)

		// 启动工作流
		startWorkFlowRequest := aggregate.StartWorkFlowRequest{
			BusinessCode:       "agenda",
			BusinessID:         "agenda-003",
			FormContent:        `{"name":"no-auto"}`,
			SponsorID:          "admin",
			WorkflowTemplateID: "MonthlyReport",
			BusinessParams: map[string]any{
				"test": "no-auto",
			},
		}
		err = clerkService.StartWorkflow(context.Background(), startWorkFlowRequest)
		convey.So(err, convey.ShouldBeNil)

		// 查询任务
		request := aggregate.GetNodeReviewTasksRequest{
			ReviewerID:   "Nash",
			BusinessCode: "agenda",
			BusinessID:   "agenda-003",
			PageCond: aggregate.PageCond{
				PageNo:   1,
				PageSize: 10,
			},
			CommonSearchCond: aggregate.CommonSearchCond{
				Search: "no-auto",
			},
		}
		tasks, total, err := clerkService.GetNodeReviewTasks(context.Background(), request)
		convey.So(err, convey.ShouldBeNil)
		convey.So(total, convey.ShouldNotEqual, 0)

		// 找到待审批的任务
		i := slices.IndexFunc(tasks, func(taskPO aggregate.NodeReviewTask) bool {
			return taskPO.Status == entity.TaskUnderReviewStatus
		})
		task := tasks[i]

		// 审批通过第一个任务
		reviewRequest := aggregate.ReviewRequest{
			Comment:            "pass",
			NodeReviewerTaskID: task.ID,
			Status:             aggregate.ReviewPassed,
		}
		err = clerkService.Review(context.Background(), reviewRequest)
		convey.So(err, convey.ShouldBeNil)

		// 查询下一个节点的任务，不应该被自动审批通过
		request.ReviewerID = "Amare"
		tasks, total, err = clerkService.GetNodeReviewTasks(context.Background(), request)
		convey.So(err, convey.ShouldBeNil)
		convey.So(total, convey.ShouldNotEqual, 0)
		for _, task := range tasks {
			if task.ReviewerID == "Nash" {
				convey.So(task.Status, convey.ShouldEqual, entity.TaskUnderReviewStatus)
			}
		}

	})
}

// TestHook 实现Hook接口的测试结构体
type TestHook struct{}

func (h *TestHook) HandleCrearedReviewTasks(ctx context.Context, businessID string, formContent string, businessCode string, nextNodeID string, params aggregate.BusinessParams, nextNodeReviewTasks []aggregate.NodeReviewTask) (err error) {
	return nil
}

func (h *TestHook) HandlePassedNode(ctx context.Context, workflowID string, nodeID string, reviewTaskID string, reviewerID string) (err error) {
	return nil
}

func (h *TestHook) HandleRejectedNode(ctx context.Context, workflowID string, nodeID string, reviewTaskID string, reviewerID string) (err error) {
	return nil
}

func (h *TestHook) HandleRejectedWorkflow(ctx context.Context, businessID, formContent string, params aggregate.BusinessParams, businessCode string) (err error) {
	return nil
}

func (h *TestHook) GetReviewerIDs(ctx context.Context, reviewerNos []string, formsContent string, params aggregate.BusinessParams) (userIDs []string, err error) {
	if slices.Index(reviewerNos, "zy_position_lasheader") > -1 {
		return []string{"Nash"}, nil
	} else {
		return []string{"Amare"}, nil
	}

}

func (h *TestHook) HandlePassedWorkflow(ctx context.Context, businessID, formContent string, params aggregate.BusinessParams, businessCode string, tasks []aggregate.NodeReviewTask) (err error) {
	return nil
}
