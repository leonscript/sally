package aggregate

import (
	"context"
	"errors"
	"slices"

	"gitee.com/leonscript/sally/domain/entity"
	"gitee.com/leonscript/sally/utils"
	"github.com/google/uuid"
	"gorm.io/gorm"
)

type Clerk struct {
	Hook
	Reference
}

func (s *Clerk) StartWorkflow(ctx context.Context, req StartWorkFlowRequest) (err error) {
	// 查询工作流模板
	workflowTemplate, err := s.Reference.QueryWorkflowTemplateByID(ctx, req.WorkflowTemplateID)
	if err != nil {
		return err
	}

	// 校验工作流合法性
	if !workflowTemplate.IsValid() {
		return errors.New("workflow template is invalid")
	}

	// 启动事务
	tx := s.Reference.Begin()
	defer func() {
		if r := recover(); r != nil {
			s.Reference.Rollback(tx)
		}

		if err != nil {
			s.Reference.Rollback(tx)
		}
	}()

	// 创建node实体
	node := entity.NewUnderReviewNode(workflowTemplate.StartNodeTemplateID, "")

	// 获取workflow实体
	workflow, err := s.getWorkflowFromStartWorkFlowRequest(ctx, req, node.ID)
	if err != nil {
		return err
	}
	// 创建workflow
	if err = s.Reference.CreateWorkflowTX(ctx, workflow, tx); err != nil {
		return err
	}

	// 保存node
	node.WorkflowID = workflow.ID
	if err = s.Reference.CreateNodeTX(ctx, node, tx); err != nil {
		return err
	}

	// 获取审批任务
	nodeReviewerTasks, _, err := s.getTargetNodeReviewerTasks(ctx, node.NodeTemplateID, node.ID, workflow)
	if err != nil {
		return err
	}
	// 创建审批任务
	if err = s.Reference.CreateReviewerTasksTX(ctx, nodeReviewerTasks, tx); err != nil {
		return err
	}

	// hook-创建下级任务后调用
	businessParams, nodeReviewTasks, err := s.getBusinessParamsAndNodeReviewTasks(ctx, workflow, nodeReviewerTasks)
	if err != nil {
		return err
	}
	if err = s.Hook.HandleCrearedReviewTasks(ctx, workflow.BusinessID, workflow.FormContent, workflow.BusinessCode, node.ID, businessParams, nodeReviewTasks); err != nil {
		return err
	}

	// 自动审批
	if err = s.autoPassReviewerTask(ctx, req.SponsorID, nodeReviewerTasks, tx); err != nil {
		return err
	}

	// 提交事务
	return s.Reference.Commit(tx)
}

// RestartWorkflow 重启审批流
func (s *Clerk) RestartWorkflow(ctx context.Context, req RestartWorkFlowRequest) (err error) {
	// 查询workflow信息
	workflow, err := s.Reference.QueryWorkflowByID(ctx, req.WorkflowID)
	if err != nil {
		return err
	}

	// 启动事务
	tx := s.Reference.Begin()
	defer func() {
		if r := recover(); r != nil {
			s.Reference.Rollback(tx)
		}

		if err != nil {
			s.Reference.Rollback(tx)
		}
	}()

	// 归档当前workflow
	workflow.Archive()
	if err = s.Reference.UpdateWorkflowTX(ctx, workflow, tx); err != nil {
		return err
	}

	// 查询workflow的template
	workflowTemplate, err := s.Reference.QueryWorkflowTemplateByID(ctx, workflow.WorkFlowTemplateID)
	if err != nil {
		return err
	}

	// 构建StartWorkflow的请求体
	var startWorkFlowRequest StartWorkFlowRequest
	if err = utils.StructCopy(ctx, &startWorkFlowRequest, &req); err != nil {
		return err
	}
	startWorkFlowRequest.WorkflowTemplateID = workflowTemplate.ID
	startWorkFlowRequest.Conclusion = workflow.Conclusion

	// 调用StartWorkflow
	if err = s.StartWorkflow(ctx, startWorkFlowRequest); err != nil {
		return err
	}

	return s.Reference.Commit(tx)
}

func (s *Clerk) UpdateWorkflowContent(ctx context.Context, workflowID, formContent string, businessParams BusinessParams) (err error) {
	// 查询workflow信息
	workflow, err := s.Reference.QueryWorkflowByID(ctx, workflowID)
	if err != nil {
		return err
	}

	// 校验
	if !workflow.IsValidToUpdateContent() {
		return errors.New("当前工作流状态不支持修改内容")
	}

	// 更新工作流
	workflow.SetFormContent(formContent)
	params, err := businessParams.Marshal()
	if err != nil {
		return errors.New("自定义参数序列化失败")
	}
	workflow.SetBusinessParams(params)
	if err = s.Reference.UpdateWorkflow(ctx, workflow); err != nil {
		return err
	}

	return err
}

func (s *Clerk) RemoveWorkflow(ctx context.Context, workflowID string) (err error) {
	// 启动事务
	tx := s.Reference.Begin()
	defer func() {
		if r := recover(); r != nil {
			s.Reference.Rollback(tx)
		}

		if err != nil {
			s.Reference.Rollback(tx)
		}
	}()

	// 删除task
	if err = s.Reference.DeleteNodeReviewerTaskByCondTX(ctx, workflowID, tx); err != nil {
		return err
	}
	// 删除node
	if err = s.Reference.DeleteNodeByCondTX(ctx, workflowID, tx); err != nil {
		return err
	}
	// 删除workflow
	if err = s.Reference.DeleteWorkflowByCondTX(ctx, workflowID, tx); err != nil {
		return err
	}

	return s.Reference.Commit(tx)
}

func (s *Clerk) Review(ctx context.Context, req ReviewRequest) (err error) {
	// 查询任务信息
	nodeReviewerTask, err := s.Reference.QueryReviewerTaskByID(ctx, req.NodeReviewerTaskID)
	if err != nil {
		return err
	}
	// 校验任务状态
	if !nodeReviewerTask.IsValidToReview() {
		return errors.New("任务已经被审批")
	}
	// 赋予任务评论
	nodeReviewerTask.Comment = req.Comment

	// 启动事务
	tx := req.tx
	if tx == nil {
		tx = s.Reference.Begin()
	}
	defer func() {
		if r := recover(); r != nil {
			s.Reference.Rollback(tx)
		}

		if err != nil {
			s.Reference.Rollback(tx)
		}
	}()

	// 通过任务id查询审批流信息，并上锁
	workflow, err := s.Reference.QueryAndLockWorkflowByCondTX(ctx, QueryWorkflowCond{
		ReviewerTaskID: req.NodeReviewerTaskID,
	}, tx)
	if err != nil {
		return err
	}

	// 校验审批流状态
	if !workflow.IsValidToReview(nodeReviewerTask.NodeID) {
		return errors.New("审批流已经被审批")
	}

	if req.IsPass() {
		// 审批通过
		if err = s.passTaskHandler(ctx, workflow, nodeReviewerTask, tx); err != nil {
			return err
		}
	} else {
		// 审批拒绝
		if err = s.rejectTaskHandler(ctx, workflow, nodeReviewerTask, tx); err != nil {
			return err
		}
	}

	// 归档该节点的其他未被审批地任务的状态
	if err = s.archiveOtherTasksOfNode(ctx, nodeReviewerTask.NodeID, tx); err != nil {
		return err
	}

	// 提交事务
	return s.Reference.Commit(tx)
}

func (s *Clerk) GetWorkflows(ctx context.Context, req GetWorkflowsRequest) (result []ActivatedWorkflow, total int64, err error) {
	// 查询workflow列表
	return s.Reference.QueryActivatedWorkflows(ctx, req)
}

func (s *Clerk) GetNodeReviewTasks(ctx context.Context, req GetNodeReviewTasksRequest) (result []NodeReviewTask, total int64, err error) {
	// 查询审批任务列表
	return s.Reference.QueryNodeReviewTasks(ctx, req)
}

func (s *Clerk) GetWorkflow(ctx context.Context, workflowID string) (result ActivatedWorkflow, err error) {
	// 查询workflow信息
	workflow, err := s.Reference.QueryWorkflowByID(ctx, workflowID)
	if err != nil {
		return result, err
	}

	// 转换为ActivatedWorkflow类型
	result = ActivatedWorkflow{
		ID:            workflow.ID,
		FormContent:   workflow.FormContent,
		CurrentNodeID: workflow.CurrentNodeID,
		Status:        workflow.Status,
		Conclusion:    workflow.Conclusion,
	}

	// 转换BusinessParams
	result.BusinessParams, err = NewBusinessParams(workflow.BusinessParams)
	return result, err
}

func (s *Clerk) getWorkflowFromStartWorkFlowRequest(ctx context.Context, startWorkFlowRequest StartWorkFlowRequest, startNodeID string) (result entity.Workflow, err error) {
	if err = utils.StructCopy(ctx, &result, &startWorkFlowRequest); err != nil {
		return result, err
	}
	// 赋予id
	uuidV7, err := uuid.NewV7()
	if err != nil {
		return result, err
	}

	result.ID = uuidV7.String()
	result.Status = entity.WorkflowUnderReviewStatus
	result.CurrentNodeID = startNodeID
	result.Conclusion = startWorkFlowRequest.Conclusion
	result.BusinessParams, err = startWorkFlowRequest.BusinessParams.Marshal()

	return result, err
}

func (s *Clerk) passTaskHandler(ctx context.Context, workflow entity.Workflow, nodeReviewerTask entity.NodeReviewerTask, tx *gorm.DB) (err error) {
	// 查询任务对应的节点信息
	node, err := s.Reference.QueryNodeByIDTX(ctx, nodeReviewerTask.NodeID, tx)
	if err != nil {
		return err
	}
	// 修改node状态为已完成
	node.Pass()
	if err = s.Reference.UpdateNodeTX(ctx, node, tx); err != nil {
		return err
	}

	// 修改任务的信息
	nodeReviewerTask.Pass()
	if err = s.Reference.UpdateNodeReviewerTaskTX(ctx, nodeReviewerTask, tx); err != nil {
		return err
	}

	//  查询nodeTemplate节点
	nodeTemplate, err := s.Reference.QueryNodeTemplateByID(ctx, node.NodeTemplateID)
	if err != nil {
		return err
	}

	// 查看node是否还有下一个节点
	if nodeTemplate.IsLastNodeOfWorkflow() {
		// node为最后一个节点，结束审批流
		// 修改workflow状态为已完成
		workflow.Pass(nodeReviewerTask.Comment)
		if err = s.Reference.UpdateWorkflowTX(ctx, workflow, tx); err != nil {
			return err
		}

		// hook-节点被审批后调用
		if err = s.HandlePassedNode(ctx, workflow.ID, nodeReviewerTask.NodeID, nodeReviewerTask.ID, nodeReviewerTask.ReviewerID); err != nil {
			return err
		}

		// hook-审批流通过后调用
		nodeReviewTasks, err := s.Reference.QueryNodeReviewTasksTX(ctx, GetNodeReviewTasksRequest{
			WorkflowID: workflow.ID,
		}, tx)
		if err != nil {
			return err
		}
		businessParams, err := NewBusinessParams(workflow.BusinessParams)
		if err != nil {
			return err
		}
		if err = s.Hook.HandlePassedWorkflow(ctx, workflow.BusinessID, workflow.FormContent, businessParams, workflow.BusinessCode, nodeReviewTasks); err != nil {
			return err
		}

	} else {
		//  如果还有下一个节点

		// 创建新的node
		nextNode := entity.NewUnderReviewNode(nodeTemplate.NextNodeTemplateID, workflow.ID)
		if err = s.Reference.CreateNodeTX(ctx, nextNode, tx); err != nil {
			return err
		}

		// 修改workflow的节点相关信息
		workflow.CurrentNodeID = nextNode.ID
		if err = s.Reference.UpdateWorkflowTX(ctx, workflow, tx); err != nil {
			return err
		}

		// hook-本节点被审批后调用
		if err = s.HandlePassedNode(ctx, workflow.ID, nodeReviewerTask.NodeID, nodeReviewerTask.ID, nodeReviewerTask.ReviewerID); err != nil {
			return err
		}

		// 创建审批任务
		nodeReviewerTasks, _, err := s.getTargetNodeReviewerTasks(ctx, nextNode.NodeTemplateID, nextNode.ID, workflow)
		if err != nil {
			return err
		}
		if err = s.Reference.CreateReviewerTasksTX(ctx, nodeReviewerTasks, tx); err != nil {
			return err
		}

		// hook-审批任务创建后调用
		businessParams, nodeReviewTasks, err := s.getBusinessParamsAndNodeReviewTasks(ctx, workflow, nodeReviewerTasks)
		if err != nil {
			return err
		}
		if err = s.Hook.HandleCrearedReviewTasks(ctx, workflow.BusinessID, workflow.FormContent, workflow.BusinessCode, nextNode.ID, businessParams, nodeReviewTasks); err != nil {
			return err
		}

		// 自动审批
		if err = s.autoPassReviewerTask(ctx, nodeReviewerTask.ReviewerID, nodeReviewerTasks, tx); err != nil {
			return err
		}

	}

	return err
}

func (s *Clerk) rejectTaskHandler(ctx context.Context, workflow entity.Workflow, nodeReviewerTask entity.NodeReviewerTask, tx *gorm.DB) (err error) {
	// 修改node状态为已驳回
	node := entity.NewRejectedNode(nodeReviewerTask.NodeID)
	if err = s.Reference.UpdateNodeTX(ctx, node, tx); err != nil {
		return err
	}

	// 修改任务的信息
	nodeReviewerTask.Reject()
	if err = s.Reference.UpdateNodeReviewerTaskTX(ctx, nodeReviewerTask, tx); err != nil {
		return err
	}

	// 修改workflow的状态
	workflow.TempReject(nodeReviewerTask.Comment)
	if err = s.Reference.UpdateWorkflowTX(ctx, workflow, tx); err != nil {
		return err
	}

	if err = s.HandleRejectedNode(ctx, workflow.ID, nodeReviewerTask.NodeID, nodeReviewerTask.ID, nodeReviewerTask.ReviewerID); err != nil {
		return err
	}

	// hook-任务被驳回通知
	businessParams, err := NewBusinessParams(workflow.BusinessParams)
	if err != nil {
		return err
	}
	return s.Hook.HandleRejectedWorkflow(ctx, workflow.BusinessID, workflow.FormContent, businessParams, workflow.BusinessCode)
}

func (s *Clerk) getTargetNodeReviewerTasks(ctx context.Context, nodeTemplateID, nodeID string, workflow entity.Workflow) (result []entity.NodeReviewerTask, reviewerIDs []string, err error) {
	// 查询审批人信息
	nodeTemplateReviewers, err := s.Reference.QueryNodeTemplateReviewers(ctx, nodeTemplateID)
	if err != nil {
		return result, reviewerIDs, err
	}

	// 使用hook查询审批人id
	businessParams, err := NewBusinessParams(workflow.BusinessParams)
	if err != nil {
		return
	}
	reviewerIDs, err = s.Hook.GetReviewerIDs(ctx, s.getReviewerNos(nodeTemplateReviewers), workflow.FormContent, businessParams)
	if err != nil {
		return result, reviewerIDs, err
	}

	// 创建审批任务
	return entity.NewNodeReviewerTasks(nodeID, reviewerIDs), reviewerIDs, err
}

func (s *Clerk) getReviewerNos(nodeTemplateReviewers []entity.NodeTemplateReviewer) (result []string) {
	for _, reviewer := range nodeTemplateReviewers {
		result = append(result, reviewer.ReviewerNo)
	}

	return result
}

func (s *Clerk) archiveOtherTasksOfNode(ctx context.Context, nodeID string, tx *gorm.DB) (err error) {
	cond := entity.NodeReviewerTask{NodeID: nodeID, Status: entity.TaskUnderReviewStatus}
	nodeReviewerTask := entity.NodeReviewerTask{Status: entity.TaskArchivedStatus}
	return s.Reference.UpdateNodeReviewerTaskByCondTX(ctx, cond, nodeReviewerTask, tx)
}

// autoPassReviewerTask 自动通过审批任务
func (s *Clerk) autoPassReviewerTask(ctx context.Context, currentUserID string, nextNodeReviewerTasks []entity.NodeReviewerTask, tx *gorm.DB) (err error) {
	// 如果下一级审批人员中包含发起人，则自动通过
	taskIndex := slices.IndexFunc(nextNodeReviewerTasks, func(nodeReviewerTask entity.NodeReviewerTask) bool {
		return nodeReviewerTask.ReviewerID == currentUserID
	})

	if taskIndex == -1 {
		return nil
	}

	// 获取需要自动审批的任务
	taskToAutoPass := nextNodeReviewerTasks[taskIndex]

	// 更新任务状态
	taskToAutoPass.Pass()
	taskToAutoPass.Comment = "系统自动审批通过"
	if err = s.Reference.UpdateNodeReviewerTaskTX(ctx, taskToAutoPass, tx); err != nil {
		return err
	}

	// 获取工作流信息
	workflow, err := s.Reference.QueryAndLockWorkflowByCondTX(ctx, QueryWorkflowCond{
		ReviewerTaskID: taskToAutoPass.ID,
	}, tx)
	if err != nil {
		return err
	}

	// 处理自动审批通过的逻辑
	return s.passTaskHandler(ctx, workflow, taskToAutoPass, tx)
}

// getBusinessParamsAndNodeReviewTasks 获取businessParams和nodeReviewTasks（领域）
func (s *Clerk) getBusinessParamsAndNodeReviewTasks(ctx context.Context, workflow entity.Workflow, nodeReviewerTasks []entity.NodeReviewerTask) (businessParams BusinessParams, nodeReviewTasks []NodeReviewTask, err error) {
	// 获取businessParams
	businessParams, err = NewBusinessParams(workflow.BusinessParams)
	if err != nil {
		return
	}

	// 转换审批任务为聚合根中的类型
	nodeReviewTasks, err = utils.ArrayCopy(ctx, NodeReviewTask{}, nodeReviewerTasks)

	// 赋值workflowID
	for i := 0; i < len(nodeReviewTasks); i++ {
		nodeReviewTasks[i].WorkflowID = workflow.ID
	}
	return
}
