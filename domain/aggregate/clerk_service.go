package aggregate

import (
	"context"
	"encoding/json"
	"time"

	"gorm.io/gorm"
)

const (
	ReviewRejected = -1
	ReviewPassed   = 1
)

// ClerkService 工作流对外服务
// 对外提供了所有工作流服务
type ClerkService interface {
	WorkflowService
	TaskService
}

/*
WorkflowService 工作流服务
提供了对工作流的基本操作
*/
type WorkflowService interface {
	// StartWorkflow 启动审批流
	// 审批流启动成功后，下一级的审批任务包含发起人，则任务会被自动审批掉
	StartWorkflow(ctx context.Context, req StartWorkFlowRequest) (err error)

	// RestartWorkflow 重启审批流
	RestartWorkflow(ctx context.Context, req RestartWorkFlowRequest) (err error)

	// UpdateWorkflowContent 更新工作流提交的内容
	UpdateWorkflowContent(ctx context.Context, workflowID, formContent string, businessParams BusinessParams) (err error)

	// RemoveWorkflow 移除工作流
	RemoveWorkflow(ctx context.Context, workflowID string) (err error)

	// GetWorkflows 获取审批流
	GetWorkflows(ctx context.Context, req GetWorkflowsRequest) (result []ActivatedWorkflow, total int64, err error)

	// GetWorkflow 根据ID获取审批流
	GetWorkflow(ctx context.Context, workflowID string) (result ActivatedWorkflow, err error)
}

/*
TaskService 任务服务
提供了对审批任务的基本操作
*/
type TaskService interface {
	// Review 审批
	// 如果下级节点的任务包含当前审批人，则会自动审批掉
	Review(ctx context.Context, req ReviewRequest) (err error)

	// GetNodeReviewTasks 获取审批任务列表
	GetNodeReviewTasks(ctx context.Context, req GetNodeReviewTasksRequest) (result []NodeReviewTask, total int64, err error)
}

type BusinessParams map[string]any

func NewBusinessParams(params string) (result BusinessParams, err error) {
	err = json.Unmarshal([]byte(params), &result)
	return result, err
}
func (b BusinessParams) Marshal() (string, error) {
	result, err := json.Marshal(b)
	return string(result), err
}

type StartWorkFlowRequest struct {
	BusinessID         string `json:"businessId"`
	BusinessCode       string `json:"businessCode"`
	WorkflowTemplateID string `json:"workflowTemplateId"`
	FormContent        string `json:"formContent"`
	SponsorID          string `json:"sponsorId"`
	Conclusion         string `json:"conclusion"`
	BusinessParams     BusinessParams
}

type RestartWorkFlowRequest struct {
	BusinessID     string `json:"businessId"`
	BusinessCode   string `json:"businessCode"`
	WorkflowID     string `json:"workflowId"`
	FormContent    string `json:"formContent"`
	SponsorID      string `json:"sponsorId"`
	BusinessParams BusinessParams
}

type ReviewerConfig struct {
	NodeTemplateID         string
	BusinessReviewDepartID string
	BusinessReviewerNo     string
}

type DBConfig struct {
	Host            string
	Port            string
	User            string
	Password        string
	Schema          string
	MaxIdleConns    int
	ConnMaxLifetime int64
	MaxOpenConns    int
}

// Hook 钩子函数 服务中会调用到的外界能力
// 要注意，钩子的调用顺序为：HandlePassedNode-HandleCrearedReviewTasks-HandlePassedWorkflow，也就是说，逻辑上会先处理本节点的钩子、下个节点审批任务的钩子、审批流结束的钩子
type Hook interface {
	// GetReviewerIDs 获取审批人id
	GetReviewerIDs(ctx context.Context, reviewerNos []string, formContent string, params BusinessParams) (userIDs []string, err error)

	// HandlePassedWorkflow 审批通过工作流
	HandlePassedWorkflow(ctx context.Context, businessID, formContent string, params BusinessParams, businessCode string, nodeReviewTasks []NodeReviewTask) (err error)

	// HandleRejectedWorkflow 驳回工作流
	HandleRejectedWorkflow(ctx context.Context, businessID, formContent string, params BusinessParams, businessCode string) (err error)

	// HandlePassedNode 整个节点被审批通过后执行
	// param reviewTaskID 节点中被审批通过的审批任务ID
	// param reviewerID 审批上面reviewTaskID任务的人员ID
	HandlePassedNode(ctx context.Context, workflowID, nodeID, reviewTaskID, reviewerID string) (err error)

	// HandlePassedNode 整个节点审批拒绝后执行
	HandleRejectedNode(ctx context.Context, workflowID, nodeID, reviewTaskID, reviewerID string) (err error)

	// HandleCrearedReviewTasks 创建审批任务后调用
	// 当一个节点审批结束后并流转到了下个节点，下个节点创建完成后，会进行下个节点的所属审批任务创建
	HandleCrearedReviewTasks(ctx context.Context, businessID, formContent, businessCode, nextNodeID string, params BusinessParams, nextNodeReviewTasks []NodeReviewTask) (err error)
}

func NewClerkService(reference Reference, hook Hook) ClerkService {
	return &Clerk{
		Reference: reference,
		Hook:      hook,
	}
}

type ReviewRequest struct {
	NodeReviewerTaskID string
	Comment            string
	Status             int

	tx *gorm.DB
}

func (r ReviewRequest) IsPass() bool {
	return r.Status == ReviewPassed
}

func (r ReviewRequest) IsRejected() bool {
	return r.Status == ReviewRejected
}

type GetWorkflowsRequest struct {
	PageCond
	CommonSearchCond
	SponsorID string

	BusinessCode string
	BusinessID   string
}

type PageCond struct {
	PageNo   int `json:"pageNo"`
	PageSize int `json:"pageSize"`
}

type CommonSearchCond struct {
	Search string `json:"search"`
}

type GetNodeReviewTasksRequest struct {
	PageCond
	CommonSearchCond
	ReviewerID string

	BusinessCode string
	BusinessID   string
	WorkflowID   string
}

type ActivatedWorkflow struct {
	ID                    string
	FormContent           string
	Name                  string
	CurrentNodeID         string
	CurrentNodeName       string
	CurrentNodeTemplateID string
	Status                int
	BusinessParams        BusinessParams
	Conclusion            string
	CreatedAt             time.Time
	UpdatedAt             time.Time
}

type NodeReviewTask struct {
	ID           string
	Comment      string
	WorkflowID   string
	WorkflowName string
	NodeID       string
	NodeName     string
	Status       int
	FormContent  string
	ReviewerID   string
	CreatedAt    time.Time
	UpdatedAt    time.Time
}
