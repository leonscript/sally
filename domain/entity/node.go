package entity

import "github.com/google/uuid"

const (
	PassedNodeStatus   = 2
	RejectedNodeStatus = -1
)

type Node struct {
	ID             string `json:"id"`
	WorkflowID     string `json:"workflowId"`
	NodeTemplateID string `json:"nodeTemplateId"`
	// Status 1:未审核 2:审核通过 -1:审核未通过
	Status int `json:"status"`
}

func NewRejectedNode(id string) Node {
	return Node{
		ID:     id,
		Status: RejectedNodeStatus,
	}
}

func NewUnderReviewNode(nodeTemplateID, workflowID string) Node {
	uuidV7, _ := uuid.NewV7()

	return Node{
		ID:             uuidV7.String(),
		NodeTemplateID: nodeTemplateID,
		WorkflowID:     workflowID,
		Status:         1,
	}
}

func (n *Node) Pass() {
	n.Status = PassedNodeStatus
}
