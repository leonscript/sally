package entity

import "github.com/google/uuid"

const (
	// TaskUnderReviewStatus 审核中
	TaskUnderReviewStatus = 1

	// TaskPassedStatus 审核通过
	TaskPassedStatus = 2

	// TaskRejectedStatus 审核未通过
	TaskRejectedStatus = -1

	// TaskArchivedStatus 被归档，因为同级别其他任务被审批，导致该任务失去意义
	TaskArchivedStatus = -10
)

type NodeReviewerTask struct {
	ID         string `json:"id"`
	NodeID     string `json:"nodeId"`
	ReviewerID string `json:"reviewerId"`
	Comment    string `json:"comment"`
	// Status 1:审核中 1:通过 -1:驳回
	Status int `json:"status"`
}

func NewNodeReviewerTasks(nodeID string, reviewerIDs []string) (result []NodeReviewerTask) {
	for _, reviewerID := range reviewerIDs {
		uuidV7, _ := uuid.NewV7()
		result = append(result, NodeReviewerTask{
			ID:         uuidV7.String(),
			NodeID:     nodeID,
			ReviewerID: reviewerID,
			Status:     1,
		})
	}
	return result
}

func (n *NodeReviewerTask) IsValidToReview() bool {
	return n.Status == TaskUnderReviewStatus
}

func (n *NodeReviewerTask) Pass() {
	n.Status = TaskPassedStatus
}

func (n *NodeReviewerTask) Reject() {
	n.Status = TaskRejectedStatus
}
