package entity

type NodeTemplate struct {
	ID   string `json:"id"`
	Name string `json:"name"`
	// NextNodeTemplateID 下一个节点的id
	NextNodeTemplateID string `json:"nextNodeTemplateID"`
	// Kind 节点类型，1:审批类型节点
	Kind int `json:"kind"`
	// WorkflowTemplateID 所属工作流模板的ID
	WorkflowTemplateID string `json:"workflowTemplateId"`
}

func (n NodeTemplate) IsLastNodeOfWorkflow() bool {
	return n.NextNodeTemplateID == ""
}
