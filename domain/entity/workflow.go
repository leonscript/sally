package entity

const (
	// WorkflowUnderReviewStatus 审批中
	WorkflowUnderReviewStatus = 1
	// WorkflowPassedReviewStatus 审批通过
	WorkflowPassedReviewStatus = 2
	// WorkflowTempRejectedStatus 临时驳回
	WorkflowTempRejectedStatus = -3
	// WorkflowArchiveStatus 归档
	WorkflowArchiveStatus = -10
)

// Workflow 审批流模板启动后成为workflow
type Workflow struct {
	ID string `json:"id"`

	// Status 审批流状态，1:审批中, 2:审批通过, -1:审批拒绝
	Status int `json:"status"`

	// WorkFlowTemplateID 审批流模板ID
	WorkFlowTemplateID string `json:"workflowTemplateId"`

	// SponsorID 发起用户ID
	SponsorID string `json:"sponsorId"`

	// FormContent 审批启动时提交的表单内容
	FormContent string `json:"formContent"`

	// BusinessID 对外关联的业务ID
	BusinessID string `json:"businessId"`

	// BusinessCode 对外关联的业务编码
	BusinessCode string `json:"businessCode"`

	// CurrentNodeID 当前审批所处的节点
	CurrentNodeID string `json:"currentNodeId"`

	// BusinessParams 业务数据
	BusinessParams string `json:"businessParams"`

	// Conclusion 工作流最后结论（当前为最后一个审批任务的评论）
	Conclusion string `json:"conclusion"`
}

func (w *Workflow) IsValidToReview(nodeID string) bool {
	if w.Status != WorkflowUnderReviewStatus {
		return false
	}

	if w.CurrentNodeID != nodeID {
		return false
	}

	return true
}

func (w *Workflow) Pass(conclusion string) {
	w.Status = WorkflowPassedReviewStatus
	w.Conclusion = conclusion
}

func (w *Workflow) TempReject(conclusion string) {
	w.Status = WorkflowTempRejectedStatus
	w.Conclusion = conclusion
}

func (w *Workflow) Archive() {
	w.Status = WorkflowArchiveStatus
}

func (w *Workflow) IsValidToUpdateContent() bool {
	return w.Status == WorkflowUnderReviewStatus
}

func (w *Workflow) SetFormContent(formContent string) {
	w.FormContent = formContent
}

func (w *Workflow) SetBusinessParams(params string) {
	w.BusinessParams = params
}
