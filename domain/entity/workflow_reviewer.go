package entity

const (
	RoleKind = 1
)

type NodeTemplateReviewer struct {
	ID string `json:"id"`
	// Kind 类型 1：角色 2：用户
	Kind           int    `json:"kind"`
	NodeTemplateID string `json:"nodeTemplateId"`
	// ReviewerNo 业务审批者编号
	ReviewerNo string `json:"reviewerNo"`
	// ReviewerDepartID 业务审批部门编号
	ReviewerDepartID string `json:"reviewerDepartID"`
}
