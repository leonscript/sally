package entity

// WorkflowTemplate 工作流模板
type WorkflowTemplate struct {
	ID   string `json:"id"`
	Name string `json:"name"`
	// StartNodeID 工作流开始的模板节点ID
	StartNodeTemplateID string `json:"startNodeTemplateId"`
}

func (w WorkflowTemplate) IsValid() bool {
	return w.ID != ""
}
