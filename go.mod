module gitee.com/leonscript/sally

go 1.22.4

require (
	github.com/google/uuid v1.6.0
	github.com/jinzhu/copier v0.4.0
	github.com/smartystreets/goconvey v1.8.1
	gorm.io/driver/mysql v1.5.7
	gorm.io/gorm v1.25.12
)

require (
	github.com/go-sql-driver/mysql v1.7.0 // indirect
	github.com/gopherjs/gopherjs v1.17.2 // indirect
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/jinzhu/now v1.1.5 // indirect
	github.com/jtolds/gls v4.20.0+incompatible // indirect
	github.com/smarty/assertions v1.15.0 // indirect
	golang.org/x/text v0.14.0 // indirect
)
