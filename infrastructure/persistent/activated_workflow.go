package persistent

import (
	"context"
	"gitee.com/leonscript/sally/domain/aggregate"
	"gitee.com/leonscript/sally/domain/entity"
	"gorm.io/gorm"
	"time"
)

type ActivatedWorkflowClient struct {
	db *gorm.DB `name:"saas_db"`
}

func NewActivatedWorkflowClient(db *gorm.DB) *ActivatedWorkflowClient {
	return &ActivatedWorkflowClient{db: db}
}

func (w *ActivatedWorkflowClient) QueryActivatedWorkflows(ctx context.Context, req aggregate.GetWorkflowsRequest) (result []ActivatedWorkflow, total int64, err error) {
	db := w.db.WithContext(ctx).
		Where("workflow.status != ?", entity.WorkflowArchiveStatus).
		Order("workflow.created_at DESC")

	if req.SponsorID != "" {
		db = db.Where("sponsor_id = ?", req.SponsorID)
	}

	if req.BusinessCode != "" {
		db = db.Where("workflow.business_code = ?", req.BusinessCode)
	}

	if req.BusinessID != "" {
		db = db.Where("workflow.business_id = ?", req.BusinessID)
	}

	if req.Search != "" {
		db = db.Where("JSON_SEARCH(workflow.form_content, 'one', ?) IS NOT NULL", "%"+req.Search+"%")
	}

	err = db.
		Select(`workflow.id,workflow.status,workflow.form_content,workflow.created_at,workflow.updated_at,workflow.current_node_id,workflow.business_params,workflow.conclusion,
        workflow_template.name,node_template.name AS current_node_name,node_template.id AS current_node_template_id`).
		Scopes(WorkflowView).
		Count(&total).Limit(req.PageSize).Offset((req.PageNo - 1) * req.PageSize).
		Find(&result).Error
	return
}

type ActivatedWorkflow struct {
	ID                    string    `gorm:"column:id;"`
	FormContent           string    `gorm:"form_content"`
	Name                  string    `gorm:"name"`
	CurrentNodeID         string    `gorm:"current_node_id"`
	CurrentNodeName       string    `gorm:"current_node_name"`
	CurrentNodeTemplateID string    `gorm:"current_node_template_id"`
	Status                int       `gorm:"status"`
	BusinessParams        string    `gorm:"business_params"`
	Conclusion            string    `gorm:"conclusion"`
	CreatedAt             time.Time `gorm:"created_at"`
	UpdatedAt             time.Time `gorm:"updated_at"`
}
