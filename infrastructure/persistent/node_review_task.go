package persistent

import (
	"context"
	"gitee.com/leonscript/sally/domain/aggregate"
	"gitee.com/leonscript/sally/domain/entity"
	"gorm.io/gorm"
	"time"
)

type NodeReviewTaskClient struct {
	db *gorm.DB `name:"saas_db"`
}

func NewNodeReviewTaskClient(db *gorm.DB) *NodeReviewTaskClient {
	return &NodeReviewTaskClient{db: db}
}

func (w *NodeReviewTaskClient) QueryNodeReviewTasks(ctx context.Context, req aggregate.GetNodeReviewTasksRequest) (result []NodeReviewTasks, total int64, err error) {
	db := w.db.WithContext(ctx).Where("node_reviewer_task.status != ?", entity.TaskArchivedStatus)

	if req.ReviewerID != "" {
		db = db.Where("node_reviewer_task.reviewer_id = ?", req.ReviewerID)
	}

	if req.BusinessCode != "" {
		db = db.Where("workflow.business_code = ?", req.BusinessCode)
	}

	if req.BusinessID != "" {
		db = db.Where("workflow.business_id = ?", req.BusinessID)
	}

	if req.WorkflowID != "" {
		db = db.Where("workflow.id = ?", req.WorkflowID)
	}

	if req.Search != "" {
		db = db.Where("JSON_SEARCH(workflow.form_content, 'one', ?) IS NOT NULL", "%"+req.Search+"%")
	}

	err = db.
		Select(`node_reviewer_task.id,node_reviewer_task.comment,node_reviewer_task.status,node_reviewer_task.created_at,node_reviewer_task.updated_at,node_reviewer_task.reviewer_id,
                      node.id AS node_id,
  				      workflow.id AS workflow_id,workflow.form_content,
                      node_template.name AS node_name,
					  workflow_template.name AS workflow_name`).
		Scopes(NodeReviewerTaskView).
		Count(&total).Limit(req.PageSize).Offset((req.PageNo - 1) * req.PageSize).
		Find(&result).Error
	return
}

func (w *NodeReviewTaskClient) QueryNodeReviewTasksTX(ctx context.Context, req aggregate.GetNodeReviewTasksRequest, tx *gorm.DB) (result []NodeReviewTasks, err error) {
	db := tx.WithContext(ctx).
		Where("node_reviewer_task.status != ?", entity.TaskArchivedStatus).
		Order("node_reviewer_task.created_at DESC")

	if req.ReviewerID != "" {
		db = db.Where("node_reviewer_task.reviewer_id = ?", req.ReviewerID)
	}

	if req.BusinessCode != "" {
		db = db.Where("workflow.business_code = ?", req.BusinessCode)
	}

	if req.BusinessID != "" {
		db = db.Where("workflow.business_id = ?", req.BusinessID)
	}

	if req.WorkflowID != "" {
		db = db.Where("workflow.id = ?", req.WorkflowID)
	}

	err = db.
		Select(`node_reviewer_task.id,node_reviewer_task.comment,node_reviewer_task.status,node_reviewer_task.created_at,node_reviewer_task.updated_at,node_reviewer_task.reviewer_id,
                      node.id AS node_id,
  				      workflow.id AS workflow_id,workflow.form_content,
                      node_template.name AS node_name,
					  workflow_template.name AS workflow_name`).
		Scopes(NodeReviewerTaskView).
		Find(&result).Error
	return
}

type NodeReviewTasks struct {
	ID           string    `gorm:"column:id;"`
	Comment      string    `gorm:"comment"`
	WorkflowID   string    `gorm:"workflow_id"`
	WorkflowName string    `gorm:"workflow_name"`
	FormContent  string    `gorm:"form_content"`
	NodeID       string    `gorm:"node_id"`
	NodeName     string    `gorm:"node_name"`
	Status       int       `gorm:"status"`
	ReviewerID   string    `gorm:"reviewer_id"`
	CreatedAt    time.Time `gorm:"created_at"`
	UpdatedAt    time.Time `gorm:"updated_at"`
}
