package persistent

import (
	"context"
	"gorm.io/gorm"
	"time"
)

const TableNameNodeTemplate = "node_template"

type NodeTemplateClient struct {
	db *gorm.DB `name:"saas_db"`
}

func NewNodeTemplateClient(db *gorm.DB) *NodeTemplateClient {
	return &NodeTemplateClient{db: db}
}

func (w *NodeTemplateClient) QueryNodeTemplateByID(ctx context.Context, id string) (result NodeTemplate, err error) {
	err = w.db.WithContext(ctx).Take(&result, "id = ?", id).Error
	return result, err
}

type NodeTemplate struct {
	ID                 string `gorm:"column:id;type:varchar(64);primaryKey;comment:Snowflake ID | 全局唯一ID"`
	Name               string `gorm:"column:name;type:varchar(64)"`
	NextNodeTemplateID string `gorm:"column:next_node_template_id;type:varchar(64)"`
	Kind               int    `gorm:"type:tinyint(5);not null;column:kind;"`
	WorkflowTemplateID string `gorm:"column:workflow_template_id;type:varchar(64)"`

	CreatedAt time.Time `gorm:"column:created_at;comment:Created Time | 创建时间" json:"created_at"`
	UpdatedAt time.Time `gorm:"column:updated_at;comment:Updated Time | 更新时间" json:"updated_at"`
}

func (w NodeTemplate) TableName() string {
	return TableNameNodeTemplate
}
