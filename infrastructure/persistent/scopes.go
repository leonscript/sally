package persistent

import "gorm.io/gorm"

func ReviewerTaskView(db *gorm.DB) *gorm.DB {
	return db.Table("node_reviewer_task").
		Joins("LEFT JOIN node ON node_reviewer_task.node_id = node.id").
		Joins("LEFT JOIN workflow ON node.workflow_id = workflow.id")
}

func WorkflowView(db *gorm.DB) *gorm.DB {
	return db.Table("workflow").
		Joins("LEFT JOIN workflow_template ON workflow.workflow_template_id = workflow_template.id").
		Joins("LEFT JOIN node ON workflow.current_node_id = node.id").
		Joins("LEFT JOIN node_template ON node.node_template_id = node_template.id")
}

func NodeReviewerTaskView(db *gorm.DB) *gorm.DB {
	return db.Table("node_reviewer_task").
		Joins("LEFT JOIN node ON node_reviewer_task.node_id = node.id").
		Joins("LEFT JOIN node_template ON node.node_template_id = node_template.id").
		Joins("LEFT JOIN workflow ON node.workflow_id = workflow.id").
		Joins("LEFT JOIN workflow_template ON workflow.workflow_template_id = workflow_template.id")
}
