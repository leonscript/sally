package persistent

import (
	"context"
	"gitee.com/leonscript/sally/domain/aggregate"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
	"time"
)

const TableNameWorkflow = "workflow"

type WorkflowClient struct {
	db *gorm.DB `name:"saas_db"`
}

func NewWorkflowClient(db *gorm.DB) *WorkflowClient {
	return &WorkflowClient{db: db}
}

func (w *WorkflowClient) QueryWorkflowByID(id string) (result Workflow, err error) {
	err = w.db.Take(&result, "id = ?", id).Error
	return result, err
}

func (w *WorkflowClient) CreateTX(ctx context.Context, po Workflow, tx *gorm.DB) (err error) {
	return tx.WithContext(ctx).Create(&po).Error
}

func (w *WorkflowClient) Update(ctx context.Context, po Workflow) (err error) {
	return w.db.WithContext(ctx).Model(&po).Updates(&po).Error
}
func (w *WorkflowClient) UpdateTX(ctx context.Context, po Workflow, tx *gorm.DB) (err error) {
	return tx.WithContext(ctx).Model(&po).Updates(&po).Error
}

func (w *WorkflowClient) LockTX(ctx context.Context, id string, tx *gorm.DB) (result Workflow, err error) {
	err = tx.WithContext(ctx).
		Clauses(clause.Locking{Strength: clause.LockingStrengthUpdate}).
		First(&result, "id = ?", id).Error

	return result, err
}

func (w *WorkflowClient) QueryWorkflowByCondTX(ctx context.Context, cond aggregate.QueryWorkflowCond, tx *gorm.DB) (result Workflow, err error) {
	if cond.ReviewerTaskID != "" {
		err = tx.WithContext(ctx).
			Select("workflow.*").
			Scopes(ReviewerTaskView).
			Where("node_reviewer_task.id = ?", cond.ReviewerTaskID).
			Take(&result).Error
	}

	return result, err
}

func (w *WorkflowClient) DeleteWorkflowByCondTX(ctx context.Context, id string, tx *gorm.DB) (err error) {
	err = tx.WithContext(ctx).Delete(&Workflow{}, "id = ?", id).Error
	return err
}

type Workflow struct {
	ID string `gorm:"column:id;type:varchar(64);primaryKey;comment:Snowflake ID | 全局唯一ID"`

	// Status 审批流状态，1:审批中, 2:审批通过, -1:审批拒绝
	Status int `gorm:"type:tinyint(5);not null;column:status;"`

	// WorkFlowTemplateID 审批流模板ID
	WorkFlowTemplateID string `gorm:"column:workflow_template_id;type:varchar(64);comment:工作流模板ID"`

	// SponsorID 发起用户ID
	SponsorID string `gorm:"column:sponsor_id;type:varchar(64)"`

	// FormContent 审批启动时提交的表单内容
	FormContent string `gorm:"column:form_content;type:json"`

	// BusinessID 对外关联的业务ID
	BusinessID string `gorm:"column:business_id;type:varchar(64);"`

	// BusinessCode 对外关联的业务编码
	BusinessCode string `gorm:"column:business_code;type:varchar(64)"`

	// CurrentNodeID 当前审批所处的节点
	CurrentNodeID string `gorm:"column:current_node_id;type:varchar(64)"`

	// BusinessParams 自定义业务数据
	BusinessParams string `gorm:"column:business_params;type:json"`

	// Conclusion 工作流审批结论
	Conclusion string `gorm:"column:conclusion;type:varchar(512)"`

	CreatedAt time.Time `gorm:"column:created_at;comment:Created Time | 创建时间" json:"created_at"`
	UpdatedAt time.Time `gorm:"column:updated_at;comment:Updated Time | 更新时间" json:"updated_at"`
}

func (w Workflow) TableName() string {
	return TableNameWorkflow
}
