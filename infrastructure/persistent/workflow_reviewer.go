package persistent

import (
	"context"
	"gorm.io/gorm"
	"time"
)

const TableNameNodeTemplateReviewer = "node_template_reviewer"

type NodeTemplateReviewerClient struct {
	db *gorm.DB `name:"saas_db"`
}

func NewNodeTemplateReviewerClient(db *gorm.DB) *NodeTemplateReviewerClient {
	return &NodeTemplateReviewerClient{db: db}
}

func (w *NodeTemplateReviewerClient) BatchCreateTX(ctx context.Context, tx *gorm.DB, po ...NodeTemplateReviewer) (err error) {
	return tx.WithContext(ctx).Create(&po).Error
}

func (w *NodeTemplateReviewerClient) QueryReviewersByNodeTemplateID(ctx context.Context, nodeTemplateID string) (result []NodeTemplateReviewer, err error) {
	err = w.db.WithContext(ctx).Where("node_template_id = ?", nodeTemplateID).Find(&result).Error
	return
}

type NodeTemplateReviewer struct {
	ID string `gorm:"column:id;type:varchar(64);primaryKey;comment:Snowflake ID | 全局唯一ID"`

	// Kind 类型 1：角色 2：用户
	Kind int `gorm:"type:tinyint(5);not null;column:kind;"`

	// NodeTemplateID 节点模板ID
	NodeTemplateID string `gorm:"column:node_template_id;type:varchar(64)"`

	ReviewerNo string `gorm:"column:reviewer_no;type:varchar(64)"`

	ReviewerDepartID string `gorm:"column:reviewer_depart_id;type:varchar(64)"`

	CreatedAt time.Time `gorm:"column:created_at;comment:Created Time | 创建时间" json:"created_at"`
	UpdatedAt time.Time `gorm:"column:updated_at;comment:Updated Time | 更新时间" json:"updated_at"`
}

func (w NodeTemplateReviewer) TableName() string {
	return TableNameNodeTemplateReviewer
}
