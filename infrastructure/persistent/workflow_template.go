package persistent

import (
	"context"
	"gorm.io/gorm"
	"time"
)

const TableNameWorkflowTemplate = "workflow_template"

type WorkflowTemplateClient struct {
	db *gorm.DB `name:"saas_db"`
}

func NewWorkflowTemplateClient(db *gorm.DB) *WorkflowTemplateClient {
	return &WorkflowTemplateClient{db: db}
}

func (w *WorkflowTemplateClient) QueryWorkflowTemplateByID(ctx context.Context, id string) (result WorkflowTemplate, err error) {
	err = w.db.WithContext(ctx).Take(&result, "id = ?", id).Error
	return result, err
}

type WorkflowTemplate struct {
	ID                  string `gorm:"column:id;type:varchar(64);primaryKey;comment:Snowflake ID | 全局唯一ID"`
	Name                string `gorm:"column:name;type:varchar(64)"`
	StartNodeTemplateID string `gorm:"column:start_node_template_id;type:varchar(64)"`

	CreatedAt time.Time `gorm:"column:created_at;comment:Created Time | 创建时间" json:"created_at"`
	UpdatedAt time.Time `gorm:"column:updated_at;comment:Updated Time | 更新时间" json:"updated_at"`
}

func (w WorkflowTemplate) TableName() string {
	return TableNameWorkflowTemplate
}
