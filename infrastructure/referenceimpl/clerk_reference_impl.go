package referenceimpl

import (
	"context"

	"gitee.com/leonscript/sally/domain/aggregate"
	"gitee.com/leonscript/sally/domain/entity"
	"gitee.com/leonscript/sally/infrastructure/persistent"
	"gitee.com/leonscript/sally/utils"
	"gorm.io/gorm"
)

type ClerkReferenceImpl struct {
	db *gorm.DB
}

func NewClerkReferenceImpl(db *gorm.DB) aggregate.Reference {
	return &ClerkReferenceImpl{db: db}
}

func (c *ClerkReferenceImpl) QueryWorkflowTemplateByID(ctx context.Context, id string) (result entity.WorkflowTemplate, err error) {
	client := persistent.NewWorkflowTemplateClient(c.db)
	workflowTemplatePO, err := client.QueryWorkflowTemplateByID(ctx, id)
	if err != nil {
		return result, err
	}

	if err = utils.StructCopy(ctx, &result, &workflowTemplatePO); err != nil {
		return result, err
	}

	return result, err
}
func (c *ClerkReferenceImpl) CreateWorkflowTX(ctx context.Context, workflow entity.Workflow, tx *gorm.DB) (err error) {
	var workflowPO persistent.Workflow
	if err = utils.StructCopy(ctx, &workflowPO, &workflow); err != nil {
		return err
	}

	client := persistent.NewWorkflowClient(c.db)
	if err = client.CreateTX(ctx, workflowPO, tx); err != nil {
		return err
	}

	return err
}

func (c *ClerkReferenceImpl) CreateNodeTX(ctx context.Context, node entity.Node, tx *gorm.DB) (err error) {
	var nodePO persistent.Node
	if err = utils.StructCopy(ctx, &nodePO, &node); err != nil {
		return err
	}

	client := persistent.NewNodeClient(c.db)
	if err = client.CreateTX(ctx, nodePO, tx); err != nil {
		return err
	}

	return err
}

func (c *ClerkReferenceImpl) CreateReviewerTasksTX(ctx context.Context, reviewerTasks []entity.NodeReviewerTask, tx *gorm.DB) (err error) {
	client := persistent.NewNodeReviewerTaskClient(c.db)
	taskPOs, err := utils.ArrayCopy(ctx, persistent.NodeReviewerTask{}, reviewerTasks)
	if err != nil {
		return err
	}

	// 创建
	return client.BatchCreateTX(ctx, tx, taskPOs...)
}

func (c *ClerkReferenceImpl) QueryReviewerTaskByID(ctx context.Context, id string) (result entity.NodeReviewerTask, err error) {
	client := persistent.NewNodeReviewerTaskClient(c.db)
	reviewerTaskPO, err := client.QueryReviewerTaskByID(ctx, id)
	if err != nil {
		return result, err
	}
	if err = utils.StructCopy(ctx, &result, &reviewerTaskPO); err != nil {
		return result, err
	}
	return result, err
}

func (c *ClerkReferenceImpl) QueryAndLockWorkflowByCondTX(ctx context.Context, cond aggregate.QueryWorkflowCond, tx *gorm.DB) (result entity.Workflow, err error) {
	client := persistent.NewWorkflowClient(c.db)
	// 查询workflow
	workflowPO, err := client.QueryWorkflowByCondTX(ctx, cond, tx)
	if err != nil {
		return result, err
	}

	// workflow上锁
	_, err = client.LockTX(ctx, workflowPO.ID, tx)
	if err != nil {
		return result, err
	}

	err = utils.StructCopy(ctx, &result, &workflowPO)
	if err != nil {
		return result, err
	}

	return result, err
}

func (c *ClerkReferenceImpl) QueryNodeTemplateByID(ctx context.Context, id string) (result entity.NodeTemplate, err error) {
	client := persistent.NewNodeTemplateClient(c.db)
	nodeTemplatePO, err := client.QueryNodeTemplateByID(ctx, id)
	if err != nil {
		return result, err
	}

	err = utils.StructCopy(ctx, &result, &nodeTemplatePO)
	return result, err
}

func (c *ClerkReferenceImpl) UpdateWorkflow(ctx context.Context, workflow entity.Workflow) (err error) {
	client := persistent.NewWorkflowClient(c.db)
	var workflowPO persistent.Workflow
	if err = utils.StructCopy(ctx, &workflowPO, &workflow); err != nil {
		return err
	}

	return client.Update(ctx, workflowPO)
}
func (c *ClerkReferenceImpl) UpdateWorkflowTX(ctx context.Context, workflow entity.Workflow, tx *gorm.DB) (err error) {
	client := persistent.NewWorkflowClient(c.db)
	var workflowPO persistent.Workflow
	if err = utils.StructCopy(ctx, &workflowPO, &workflow); err != nil {
		return err
	}

	return client.UpdateTX(ctx, workflowPO, tx)
}

func (c *ClerkReferenceImpl) UpdateNodeTX(ctx context.Context, node entity.Node, tx *gorm.DB) (err error) {
	client := persistent.NewNodeClient(c.db)
	var nodePO persistent.Node
	if err = utils.StructCopy(ctx, &nodePO, &node); err != nil {
		return err
	}

	return client.UpdateTX(ctx, nodePO, tx)
}

func (c *ClerkReferenceImpl) QueryNodeTemplateReviewers(ctx context.Context, nodeTemplateID string) (result []entity.NodeTemplateReviewer, err error) {
	client := persistent.NewNodeTemplateReviewerClient(c.db)
	reviewers, err := client.QueryReviewersByNodeTemplateID(ctx, nodeTemplateID)
	if err != nil {
		return result, err
	}

	return utils.ArrayCopy(ctx, entity.NodeTemplateReviewer{}, reviewers)
}

func (c *ClerkReferenceImpl) QueryNodeByIDTX(ctx context.Context, id string, tx *gorm.DB) (result entity.Node, err error) {
	client := persistent.NewNodeClient(c.db)
	nodePO, err := client.QueryNodeByIDTX(ctx, id, tx)
	if err != nil {
		return result, err
	}

	err = utils.StructCopy(ctx, &result, &nodePO)
	return result, err
}

func (c *ClerkReferenceImpl) UpdateNodeReviewerTaskTX(ctx context.Context, nodeReviewerTask entity.NodeReviewerTask, tx *gorm.DB) (err error) {
	client := persistent.NewNodeReviewerTaskClient(c.db)
	var nodeReviewerTaskPO persistent.NodeReviewerTask
	if err = utils.StructCopy(ctx, &nodeReviewerTaskPO, &nodeReviewerTask); err != nil {
		return err
	}

	return client.UpdateTX(ctx, nodeReviewerTaskPO, tx)
}

func (c *ClerkReferenceImpl) UpdateNodeReviewerTaskByCondTX(ctx context.Context, cond, nodeReviewerTask entity.NodeReviewerTask, tx *gorm.DB) (err error) {
	client := persistent.NewNodeReviewerTaskClient(c.db)
	// 构建条件
	var condPO persistent.NodeReviewerTask
	if err = utils.StructCopy(ctx, &condPO, &cond); err != nil {
		return err
	}

	// 构建修改值
	var nodeReviewerTaskPO persistent.NodeReviewerTask
	if err = utils.StructCopy(ctx, &nodeReviewerTaskPO, &nodeReviewerTask); err != nil {
		return err
	}

	// 执行更新
	return client.UpdateByCondTX(ctx, condPO, nodeReviewerTaskPO, tx)
}

func (c *ClerkReferenceImpl) QueryActivatedWorkflows(ctx context.Context, req aggregate.GetWorkflowsRequest) (result []aggregate.ActivatedWorkflow, total int64, err error) {
	client := persistent.NewActivatedWorkflowClient(c.db)
	activatedWorkflowPOs, total, err := client.QueryActivatedWorkflows(ctx, req)
	if err != nil {
		return result, total, err
	}

	result, err = utils.ArrayCopy(ctx, aggregate.ActivatedWorkflow{}, activatedWorkflowPOs)
	if err != nil {
		return result, total, err
	}

	// 数据转换
	result, err = c.getActivatedWorkflows(activatedWorkflowPOs)

	return result, total, err
}

func (c *ClerkReferenceImpl) QueryWorkflowByID(ctx context.Context, id string) (result entity.Workflow, err error) {
	client := persistent.NewWorkflowClient(c.db)
	workflowPO, err := client.QueryWorkflowByID(id)
	if err != nil {
		return result, err
	}

	err = utils.StructCopy(ctx, &result, &workflowPO)
	return result, err
}

func (c *ClerkReferenceImpl) QueryNodeReviewTasks(ctx context.Context, req aggregate.GetNodeReviewTasksRequest) (result []aggregate.NodeReviewTask, total int64, err error) {
	client := persistent.NewNodeReviewTaskClient(c.db)
	taskPOs, total, err := client.QueryNodeReviewTasks(ctx, req)
	if err != nil {
		return result, total, err
	}

	result, err = utils.ArrayCopy(ctx, aggregate.NodeReviewTask{}, taskPOs)
	return result, total, err
}

func (c *ClerkReferenceImpl) QueryNodeReviewTasksTX(ctx context.Context, req aggregate.GetNodeReviewTasksRequest, tx *gorm.DB) (result []aggregate.NodeReviewTask, err error) {
	client := persistent.NewNodeReviewTaskClient(c.db)
	taskPOs, err := client.QueryNodeReviewTasksTX(ctx, req, tx)
	if err != nil {
		return result, err
	}

	result, err = utils.ArrayCopy(ctx, aggregate.NodeReviewTask{}, taskPOs)
	return result, err
}

func (c *ClerkReferenceImpl) DeleteWorkflowByCondTX(ctx context.Context, id string, tx *gorm.DB) (err error) {
	return persistent.NewWorkflowClient(c.db).DeleteWorkflowByCondTX(ctx, id, tx)
}

func (c *ClerkReferenceImpl) DeleteNodeByCondTX(ctx context.Context, workflowID string, tx *gorm.DB) (err error) {
	return persistent.NewNodeClient(c.db).DeleteNodeByCondTX(ctx, workflowID, tx)
}

func (c *ClerkReferenceImpl) DeleteNodeReviewerTaskByCondTX(ctx context.Context, workflowID string, tx *gorm.DB) (err error) {
	// 查询workflow下的节点
	nodes, err := persistent.NewNodeClient(c.db).QueryNodesByCondTX(ctx, persistent.Node{WorkflowID: workflowID}, tx)
	if err != nil {
		return err
	}

	// 删除任务
	return c.deleteReviewerTaskByNodePOs(ctx, nodes, tx)
}

func (c *ClerkReferenceImpl) Begin() *gorm.DB {
	return c.db.Begin()
}

func (c *ClerkReferenceImpl) Commit(tx *gorm.DB) error {
	return tx.Commit().Error
}

func (c *ClerkReferenceImpl) Rollback(tx *gorm.DB) error {
	return tx.Rollback().Error
}

func (c *ClerkReferenceImpl) getActivatedWorkflows(activatedWorkflowPOs []persistent.ActivatedWorkflow) (result []aggregate.ActivatedWorkflow, err error) {
	for _, activatedWorkflowPO := range activatedWorkflowPOs {
		activatedWorkflow := aggregate.ActivatedWorkflow{
			CreatedAt:             activatedWorkflowPO.CreatedAt,
			CurrentNodeID:         activatedWorkflowPO.CurrentNodeID,
			CurrentNodeName:       activatedWorkflowPO.CurrentNodeName,
			CurrentNodeTemplateID: activatedWorkflowPO.CurrentNodeTemplateID,
			FormContent:           activatedWorkflowPO.FormContent,
			Conclusion:            activatedWorkflowPO.Conclusion,
			ID:                    activatedWorkflowPO.ID,
			Name:                  activatedWorkflowPO.Name,
			Status:                activatedWorkflowPO.Status,
			UpdatedAt:             activatedWorkflowPO.UpdatedAt,
		}
		activatedWorkflow.BusinessParams, err = aggregate.NewBusinessParams(activatedWorkflowPO.BusinessParams)
		if err != nil {
			return result, err
		}
		result = append(result, activatedWorkflow)
	}

	return result, nil
}

func (c *ClerkReferenceImpl) deleteReviewerTaskByNodePOs(ctx context.Context, nodes []persistent.Node, tx *gorm.DB) (err error) {
	var nodeIDs []string
	for _, node := range nodes {
		nodeIDs = append(nodeIDs, node.ID)
	}

	return persistent.NewNodeReviewerTaskClient(c.db).DeleteNodeReviewerTaskByCondTX(ctx, nodeIDs, tx)
}
