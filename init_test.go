package sally

import (
	"github.com/smartystreets/goconvey/convey"
	"testing"
)

func TestInit(t *testing.T) {
	convey.Convey("test init database", t, func() {
		db, err := connect(Config{
			Host:     "192.168.110.242",
			Port:     "3306",
			User:     "root",
			Password: "Ul6WI12AuZomj76Kvl700-",
			Schema:   "sally",
		})

		_, err = Init(db, nil)

		convey.So(err, convey.ShouldBeNil)
	})
}
